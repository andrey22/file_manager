<?
    require 'FileInfoModel.php';

    final class FileManager
    {
        public function get($path, $order = 0) 
        {
            $files = scandir($path, $order);
            $fileInfoModels = array();            

            foreach($files as $file)
            {
                if($file == "." || $file == "..")
                {
                    continue;
                }

                $filePath = $path . DIRECTORY_SEPARATOR . $file;
                $isDir = is_dir($filePath);
                $fileExtension = "directory";
                $fileLength = 0;
                if(!$isDir)
                {
                    $fileExtension = pathinfo($filePath, PATHINFO_EXTENSION);
                    $fileLength = filesize($filePath);
                }

                $fileInfoModels[] = new FileInfoModel($file, $fileExtension, $fileLength, $filePath, $isDir);
            }

            return $fileInfoModels;
        }

        public function sortByLength(string $path, int $order = 0): array
        {
            return $this->sortBy($path, $this->createSortFunctionByProperty('length', $order));
        }

        public function sortByExtension($path, $order = 0)
        {
            return $this->sortBy($path, $this->createSortFunctionByProperty('extension', $order));
        }

        public function sortBy(string $path, callable $sortFunction): array
        {
            return $sortFunction($this->get($path));
        }

        private function createSortFunctionByProperty(string $property, int $order) : \Closure
        {
            return function(array $items) use ($property, $order): array {
                $ascFunction = function($a, $b) use ($property) { return $a->$property <=> $b->$property; };
                $descFunction =  function($a, $b) use ($property) { return $b->$property <=> $a->$property; };
                usort($items, $order ? $descFunction: $ascFunction);
                return $items;
            };
        }

        private static $instance;

        public static function getInstance()
        {
            if (null === static::$instance)
            {
                static::$instance = new static();
            }
    
            return static::$instance;
        }

        private function __construct()
        {
        }
    
        private function __clone()
        {
        }

        private function __wakeup()
        {
        }
    }
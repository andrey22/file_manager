<?
    class FileInfoModel
    {
        public $fileName = "";
        public $extension = "";
        public $length = 0;
        public $path = "";
        public $isDir = false;

        public function __construct($fileName, $extension, $length, $path, $isDir)
        {
            $this->fileName = $fileName;
            $this->extension = $extension;
            $this->length = $length;
            $this->path = $path;
            $this->isDir = $isDir;
        }
    }
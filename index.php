<html>
<head>
    <link href="bootstrap3/css/bootstrap.min.css" rel="stylesheet"/>
    <script src="bootstrap3/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <hr/>
        <?
            require 'FileManager.php';
            require 'FileManagerView.php';

            ini_set("display_errors", "1");
            error_reporting(E_ALL);

            //header("Content-Type: text/html; charset=utf-8");

            $fileManager = FileManager::getInstance();
            $fileManagerView = new FileManagerView();
            $defaultPath = getcwd();

            if(!empty($_GET['path']))
            {
                $pathArray = explode("\\", $_GET['path']);

                echo "<p>";

                foreach ($pathArray as $pathPart)
                {
                    $directoryPath = "";
                    foreach ($pathArray as $pathChunk) {
                        if($pathPart == $pathChunk)
                        {
                            break;
                        }
                        $directoryPath .= $pathChunk . DIRECTORY_SEPARATOR;
                    }
                    echo "<a href=\"index.php?path=".$directoryPath.$pathPart."\" class=\"btn btn-default btn-xs\" role=\"button\">".$pathPart."</a>";
                }

                echo "</p><hr/>";

                $fileManagerView->render($fileManager->get($_GET['path']));
                return;
            }
            else
            {
                $fileManagerView->render($fileManager->get($defaultPath));
                return;
            }

            if($_GET['sortby'] && $_GET['order'])
            {
                if($_GET['sortby'] == "length" && $_GET['order'] == "asc")
                {
                    $fileManagerView->render($fileManager->sortByLength($defaultPath, 0));
                }
                else if($_GET['sortby'] == "length" && $_GET['order'] == "desc")
                {
                    $fileManagerView->render($fileManager->sortByLength($defaultPath, 1));
                }

                if($_GET['sortby'] == "extension" && $_GET['order'] == "asc")
                {
                    $fileManagerView->render($fileManager->sortByExtension($defaultPath, 0));
                }
                if($_GET['sortby'] == "extension" && $_GET['order'] == "desc")
                {
                    $fileManagerView->render($fileManager->sortByExtension($defaultPath, 1));
                }
            }

            //echo("<PRE>");
            //print_r($fileManager->sortByExtension($defaultPath, 0));
        ?>
    </div>
</div>
</body>
</html>
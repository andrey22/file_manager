<table class="table table-bordered">
    <tr>
        <th>FileName</th>
        <th>Length</th>
        <th>Extension</th>
        <th>Path</th>
    </tr>
    <? foreach($items as $item): ?>
        <tr>
            <td>
                <?
                if($item->isDir) {
                    echo "<a href=\"index.php?path=".$item->path."\">".$item->fileName."</a>";
                }
                else {
                    echo $item->fileName;
                }
                ?>
            </td>
            <td> <?= $item->length; ?></td>
            <td> <?= $item->extension; ?></td>
            <td> <?= $item->path; ?></td>
        </tr>
    <? endforeach; ?>
</table>